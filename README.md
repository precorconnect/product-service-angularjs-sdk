## Description
An AngularJS Module implementing common use cases encountered when integrating AngularJS apps
 with the precorconnect product service.

## UseCases

#####listCurrentPartnerProducts
Lists the current partners products

#####listProductGroups
Lists all product groups

#####listProductLines
Lists all product lines

#####searchForProductWithSerialNumber
Searches for a product with a given serial number

## Installation
add as bower dependency

```shell
bower install https://bitbucket.org/precorconnect/product-service-angularjs-sdk.git --save
```
include in view
```html
<script src="bower-components/angular/angular.js"></script>
<script src="bower-components/product-service-angularjs-sdk/dist/product-service-angularjs-sdk.js"></script>
```
configure
see below.

## Configuration
####Properties
| Name (* denotes required) | Description |
|------|-------------|
| baseUrl* | The base url of the product service. |

#### Example
```js
angular.module(
        "app",
        ["productServiceModule"])
        .config(
        [
            "productServiceConfigProvider",
            appConfig
        ]);

    function appConfig(productServiceConfigProvider) {
        productServiceConfigProvider
            .setBaseUrl("@@productServiceBaseUrl");
    }
```