module.exports = function (grunt) {
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.initConfig({
        concat:{
            dist: {
                src: ['src/product-service.module.js','src/*.js'],
                dest: 'dist/product-service-angularjs-sdk.js'
            }
        }
    });
    grunt.registerTask('default', ['concat']);
};