(function () {
    angular
        .module('productServiceModule')
        .factory(
        'productServiceClient',
        [
            'productServiceConfig',
            '$http',
            '$q',
            productServiceClient
        ]);

    function productServiceClient(productServiceConfig,
                                  $http,
                                  $q) {

        var thisFactory = this;
        thisFactory.cachedProductLines = null;
        thisFactory.cachedProductGroups = null;

        return {
            listCurrentPartnerProducts: listCurrentPartnerProducts,
            listProductGroups: listProductGroups,
            listProductLines: listProductLines,
            searchForProductWithSerialNumber: searchForProductWithSerialNumber
        };

        /**
         * @typedef {Object} ProductGroupView
         * @property {string} name
         * @property {number} id
         */

        /**
         * @typedef {Object} ProductLineView
         * @property {string} name
         * @property {number} id
         * @property {ProductGroupView} productGroup
         */

        /**
         * A summary of a product
         * @typedef {Object} ProductSummary
         * @property {string} serialNumber
         * @property {ProductLineView} productLine
         * @property {string} description
         */

        /**
         * Lists the current partners products
         * @returns a promise of {ProductSummary[]}
         */
        function listCurrentPartnerProducts() {
            // use dummy data for now
            var deferred = $q.defer();
            deferred.resolve([
                {
                    serialNumber: "AB53G05110010",
                    productLine: {
                        name: "Bike Display - Commercial",
                        id: 13,
                        productGroup: {
                            name: "Bike Display - Commercial",
                            id: 12
                        }
                    },
                    description: "some description 1"
                },
                {
                    serialNumber: "AB53G07110012",
                    productLine: {
                        name: "Bike Display - Commercial",
                        id: 13,
                        productGroup: {
                            name: "Bike Display - Commercial",
                            id: 12
                        }
                    },
                    description: "some description 2"
                },
                {
                    serialNumber: "AB37G11110053",
                    productLine: {
                        name: "Treadmill Base - Commercial",
                        id: 90,
                        productGroup: {
                            name: "Treadmill - Commercial",
                            id: 1
                        }
                    },
                    description: "some description 3"
                }
            ]);
            return deferred.promise;
            /*
             var request = $http({
             method: "get",
             url: productServiceConfig.baseUrl + "/product-types"
             });

             return request
             .then(
             handleSuccess,
             handleError
             );
             */
        }

        /**
         * Lists all product groups
         * @returns a promise of {ProductGroupView[]}
         */
        function listProductGroups() {

            if (thisFactory.cachedProductGroups) {
                var deferred = $q.defer();
                deferred.resolve(thisFactory.cachedProductGroups);
                return deferred.promise;
            }
            else {
                return $http({
                    method: "get",
                    url: productServiceConfig.baseUrl + "/product-groups"
                })
                    .then(function (response) {
                        // cache
                        thisFactory.cachedProductGroups = response.data;
                        return thisFactory.cachedProductGroups;
                    });
            }

        }

        /**
         * Lists all product lines
         * @returns a promise of {ProductLineView[]}
         */
        function listProductLines() {

            if (thisFactory.cachedProductLines) {
                var deferred = $q.defer();
                deferred.resolve(thisFactory.cachedProductLines);
                return deferred.promise;
            }
            else {
                return $http({
                    method: "get",
                    url: productServiceConfig.baseUrl + "/product-lines"
                })
                    .then(function (response) {
                        // cache
                        thisFactory.cachedProductLines = response.data;
                        return thisFactory.cachedProductLines;
                    });
            }

        }

        /**
         * Searches for a product with a given serial number
         * @param serialNumber
         * @returns {ProductSummary|null}
         */
        function searchForProductWithSerialNumber(serialNumber) {
            // use dummy data for now
            var deferred = $q.defer();
            var product = null;
            if (serialNumber == 1) {
                product = {
                    serialNumber: serialNumber,
                    productLine: {
                        name: "Treadmill Base - Commercial",
                        id: 90,
                        productGroup: {
                            name: "Treadmill - Commercial",
                            id: 1
                        }
                    },
                    description: "some description"
                }
            }
            deferred.resolve(product);
            return deferred.promise;

            /*
             var request = $http({
             method: "get",
             url: productServiceConfig.baseUrl + "/assets",
             params: {
             serialNumber: serialNumber
             }
             });

             return request
             .then(
             handleSuccess,
             handleError
             );
             */
        }
    }
})
();
